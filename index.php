<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("button").click(function () {
                $("p").html("Hello <b>world!</b>");
                $('.container2').hide();
            });

        });

    </script>
    <title>Inicio trabajo</title>
</head>
<body>


    <header>
        <nav>
            <ul>
                <li><a class="active">ingreso</a></li>
                <li><a href="funciones.php">Funciones PHP</a></li>
                <li><a href="ejem.php">Ejemplos</a></li>

            </ul>
        </nav>
    </header>
    <button>hide windows</button>
    <div class="container">
        <p>hola todos</p>
    </div>
    <div class="container2">
        <h1>User</h1>
        <?php require "conectar.php"; ?>
    </div>
    <div class="container2">
        <h1>SHOP</h1>
        <?php require "shop.php"; ?>
    </div>
    <div class="container2">
            <h1>CARS</h1>
        	<?php require "carro.php"; ?>
    </div>
    <footer class="footer">
        <p>Made by Brenda Yoko Duran</p>
    </footer>
</body>
</html>