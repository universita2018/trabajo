-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-05-2018 a las 21:54:35
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cliente`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `car`
--

CREATE TABLE `car` (
  `idCar` int(11) NOT NULL,
  `nameCar` char(20) DEFAULT NULL,
  `typeCar` char(20) DEFAULT NULL,
  `modelCar` char(11) DEFAULT NULL,
  `colorCar` char(11) NOT NULL,
  `motorCar` int(5) DEFAULT NULL,
  `prizeCar` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `car`
--

INSERT INTO `car` (`idCar`, `nameCar`, `typeCar`, `modelCar`, `colorCar`, `motorCar`, `prizeCar`, `stock`) VALUES
(1, 'Pathfinder2000', 'Jeep', 'Mitsubishi', 'White', 1000, 150000, 25),
(2, 'Pathfinder2007', 'Jeep', 'Mitsubishi', 'red', 1000, 145000, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carshop`
--

CREATE TABLE `carshop` (
  `idcarShop` int(11) NOT NULL,
  `idcar` int(11) DEFAULT NULL,
  `idShop` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `name` char(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` char(50) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `mail` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`id`, `name`, `age`, `address`, `phone`, `mail`) VALUES
(1, 'P', 25, 'L', 4444444, 'e'),
(2, 'P', 25, 'L', 4444444, 'e'),
(3, 'P', 25, 'L', 4444444, 'e'),
(4, 'P', 25, 'L', 4444444, 'e'),
(5, 'P', 25, 'L', 4444444, 'e'),
(6, 'Paulo', 25, 'Lima', 4444444, 'exam@hotmail.com'),
(7, 'Paulo', 25, 'Lima', 4444444, 'exam@hotmail.com'),
(8, 'Paulo', 25, 'Lima', 4444444, 'exam@hotmail.com'),
(9, 'Teddie', 35, 'La PAz', 4258477, 'tedd@hotmail.com'),
(10, 'Tedd', 25, 'La PAz', 4258476, 'tedexp@hotmail.com'),
(11, 'Tedd', 25, 'La PAz', 4258476, 'tedexp@hotmail.com'),
(12, 'Tedd', 25, 'La PAz', 4258476, 'tedexp@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shop`
--

CREATE TABLE `shop` (
  `idShop` int(11) NOT NULL,
  `idPerson` int(11) NOT NULL,
  `nameShop` char(10) DEFAULT NULL,
  `stockShop` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `shop`
--

INSERT INTO `shop` (`idShop`, `idPerson`, `nameShop`, `stockShop`, `price`, `total`) VALUES
(1, 1, 'cars234', 2, 150000, 350000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`idCar`);

--
-- Indices de la tabla `carshop`
--
ALTER TABLE `carshop`
  ADD PRIMARY KEY (`idcarShop`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`idShop`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `car`
--
ALTER TABLE `car`
  MODIFY `idCar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `carshop`
--
ALTER TABLE `carshop`
  MODIFY `idcarShop` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `shop`
--
ALTER TABLE `shop`
  MODIFY `idShop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
